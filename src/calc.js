// TODO: arithmetic operations
/**
 * 
 * @param {number} a 
 * @param {number} b 
 * @returns {number}
 * @throws {Error} 0 division
 */
const add = (a, b) => a + b;

const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

/**
 * Divides two numbers.
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed");
        const fraction = dividend / divisor;
        return fraction;
};

export default { add, subtract, multiply, divide }
